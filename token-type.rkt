#lang racket/base

(provide (prefix-out tok: (except-out (all-defined-out)
                                      define-token-type)))

(define-syntax-rule (define-token-type name)
  (define name 'name))

; Single-character tokens.
(define-token-type left-paren)
(define-token-type right-paren)
(define-token-type left-brace)
(define-token-type right-brace)
(define-token-type comma)
(define-token-type dot)
(define-token-type minus)
(define-token-type plus)
(define-token-type semicolon)
(define-token-type slash)
(define-token-type star)

; One or two character tokens.
(define-token-type bang)
(define-token-type bang-equal)
(define-token-type equal)
(define-token-type equal-equal)
(define-token-type greater)
(define-token-type greater-equal)
(define-token-type less)
(define-token-type less-equal)

; Literals.
(define-token-type identifier)
(define-token-type string)
(define-token-type number)

; Keywords.
(define-token-type and)
(define-token-type class)
(define-token-type else)
(define-token-type false)
(define-token-type fun)
(define-token-type for)
(define-token-type if)
(define-token-type nil)
(define-token-type or)
(define-token-type print)
(define-token-type return)
(define-token-type super)
(define-token-type this)
(define-token-type true)
(define-token-type var)
(define-token-type while)

(define-token-type eof)
