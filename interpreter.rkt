#lang racket/base

;; (provide
;;  (contract-out
;;   [struct interpreter ((globals environment?)
;;                        (environment environment?))]))
;;
;; TODO use contracts + submodule when https://github.com/racket/racket/issues/4661 is fixed
(provide (struct-out interpreter))

(struct interpreter (globals
                     locals
                     [environment #:mutable]))
