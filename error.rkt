#lang racket/base

(require racket/contract
         "token.rkt"
         "token-type.rkt")

(provide
 (contract-out
  [struct exn:fail:lox:runtime-error ((message string?)
                                      (continuation-marks continuation-mark-set?)
                                      (token token?))])
 (prefix-out
  lox:
  (contract-out
   [had-error? (-> boolean?)]
   [set-error! (boolean? . -> . void?)]
   [report-line-error ((and/c integer? positive?) string? . -> . void?)]
   [report-token-error (token? string? . -> . void?)]
   [had-runtime-error? (-> boolean?)]
   [raise-runtime-error (token? string? . -> . any)]
   [report-runtime-error (exn? . -> . any)])))

(struct exn:fail:lox:runtime-error exn:fail (token)
  #:extra-constructor-name make-exn:fail:lox:runtime-error
  #:transparent)

(define (raise-runtime-error token message)
  (raise (exn:fail:lox:runtime-error
          message
          (current-continuation-marks)
          token)))

(define had-error #false)
(define had-runtime-error #false)

(define (had-error?) had-error)
(define (had-runtime-error?) had-runtime-error)

(define (set-error! value)
  (set! had-error value))

(define (report-line-error line message)
  (report-error line "" message))

(define (report-token-error token message)
  (if (equal? (token-type token) tok:eof)
      (report-error (token-line token) " at end" message)
      (report-error (token-line token) (format " at '~a'" (token-lexeme token)) message)))

(define (report-error line where message)
  (eprintf "[line ~a] Error~a: ~a~n" line where message)
  (set! had-error #true))

(define (report-runtime-error err)
  (eprintf "~a\n" (exn-message err))
  (eprintf "[line ~a]\n" (token-line (exn:fail:lox:runtime-error-token err)))
  (set! had-runtime-error #true))
